# HDF_Sbull驱动
HDF Simple Block Utility for Loading Localities

Date: 2021-9-4  
Author: yhuan416  

2021-9-4    
    初次上传,添加使用说明

---

## 目标  
- 使用内存模拟flash  
- 支持HDF框架指定内存位置与大小  
- 不对源码做任何修改或少量修改
- 支持挂载根文件系统

# 如何使用

*ps:以下代码基于ohos主线进行操作*

## 1. 添加一块MMU内存映射  

```
    {
        .phys = DDR_RAMFS_ADDR,
        .virt = DDR_RAMFS_VBASE,
        .size = DDR_RAMFS_SIZE,
        .flags = MMU_INITIAL_MAP_DEVICE,
        .name = "Sbull",
    },
```

需要自行计算该区域的虚拟内存起始地址  
此处的虚拟内存首地址为 0xc9000000

*ps: 如果是旧版的mmu映射代码,请自行添加映射*

## 2. 下载驱动源码

驱动保存路径
> device/${company}/drivers/sbull   
``` bash
$ cd device/st/drivers 
$ git clone git@gitee.com:yhuan416/hdf_sbull.git sbull
```

## 3. 将驱动加入编译

修改同目录下的lite.mk文件,添加驱动
> device/${company}/drivers/lite.mk  
```
# sbull
LIB_SUBDIRS     += $(STM32MP157_BASE_DIR)/sbull
LITEOS_BASELIB += -lhdf_sbull
```

*ps: 此处以makefile编译框架做示范,后续添加gn框架的编译方法*

## 4. 添加hcs配置

路径:
> device/st/stm32mp1/liteos_a/hdf_config/sbull/sbull_config.hcs  

```
root {
    module = "sbull";
    platform {
        template sbull_controller {
            match_attr = "sbull_0";
            vbase = 0x0;
            size = 0x1000000;
        }
        controller_0xD0000000 :: sbull_controller {
            vbase = 0xc9000000;
            size = 0x1000000;
            match_attr = "sbull_0";
        }
    }
}
```


路径:
> device/st/stm32mp1/liteos_a/hdf_config/device_info/device_info.hcs  

添加以下节点
```
        storage :: host {
            hostName = "storage_host";
            sbull_flash :: device {
                sbull_flash0 :: deviceNode {
                    policy = 0;
                    priority = 50;
                    permission = 0660;
                    moduleName = "sbull";
                    serviceName = "HDF_PLATFORM_SBULL_0";
                    deviceMatchAttr = "sbull_0";
                }
            }
        }
```

## 5. 启动时添加分区并挂载根文件系统

路径:
> device/st/stm32mp1/liteos_a/board/os_adapt/os_adapt.c

``` c
int mount_rootfs(void)
{
    int ret = 0;

    // 添加分区
    // 0x800000:根文件系统分区的大小
    ret = add_mtd_partition("spinor", 0, 0x800000, 0);
    if (ret) {
        dprintf("add_mtd_partition fail.");
        return ret;
    }

    // 挂载
    dprintf("mount /dev/spinorblk0 / ...\n");
    if (mount("/dev/spinorblk0", "/", "jffs2", 0, NULL))
    {
        PRINT_ERR("mount failed.\n");
    }

    return 0;
}

void SystemInit(void)
{
...

// 需要先初始化hdf驱动框架
#ifdef LOSCFG_DRIVERS_HDF
	dprintf("DeviceManagerStart start ...\n");
	if (DeviceManagerStart()) {
		PRINT_ERR("No drivers need load by hdf manager!");
	}
	dprintf("DeviceManagerStart end ...\n");
#endif

// 挂载根文件系统
#ifdef LOSCFG_PLATFORM_ROOTFS
    dprintf("OsMountRootfs start ...\n");
    mount_rootfs();
    dprintf("OsMountRootfs end ...\n");
#endif

...
}
```

## 6. 构建

``` bash
$ make CONFIG=.config_stm32mp157 TEE=1 clean
$ make CONFIG=.config_stm32mp157 TEE=1 liteos
$ make CONFIG=.config_stm32mp157 TEE=1 FSTYPE=jffs2 rootfs
```

*ps: 制作根文件系统时需要指定其大小*
*[参考文档](https://gitee.com/yhuan416/st/blob/master/doc/%E7%A7%BB%E6%A4%8D%E6%97%A5%E5%BF%97.md)*

## 7. 烧录启动

``` bash
$ mw.b 0xc0100000 0xff 0xDC000
$ tftpboot 0xc0100000 liteos_with_uboot_header.bin
$ mw.b 0xD0000000 0xff 0x1000000
$ tftpboot 0xD0000000 rootfs_jffs2.img
$ bootm 0xc0100000
```

# TODO
- 完善字符设备相关函数  
...

---
有使用问题欢迎提issue~  
