# Copyright (c) 2020-2021 YangZeLin. All rights reserved.

include $(LITEOSTOPDIR)/config.mk
include $(LITEOSTOPDIR)/../../drivers/adapter/khdf/liteos/lite.mk

MODULE_NAME := hdf_sbull

LOCAL_CFLAGS += $(HDF_INCLUDE) 

LOCAL_SRCS := 	hdf_sbull.c \
				sbull.c \
				sbullMtd.c \
				sbullOps.c

include $(HDF_DRIVER)
