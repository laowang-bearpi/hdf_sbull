#ifndef _SBULL_H_
#define _SBULL_H_

#include "stdio.h"
#include "stdint.h"

#define HDF_LOG_TAG sbull

// 扇区大小 4K
#define SBULL_SECTOR_SIZE   (64 * 1024)

/*
sbull模块通用定义
*/
struct Sbull
{
    uint8_t *base;  // 虚拟内存基地址
    uint32_t size;  // 虚拟内存大小
};

int32_t sbullInit(struct Sbull *sbull, uint32_t vbase, uint32_t size);

#endif
